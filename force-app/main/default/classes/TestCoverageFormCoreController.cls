/**
 * Name: TestCoverageFormCoreController
 * Description: Contains the test methods to test CoverageFormCoreController
 *
 * Confidential & Proprietary, 2011 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
@isTest(seeAlldata = false)
private class TestCoverageFormCoreController 
    {
	private static final Tier1CRMTestDataFactory dataFactory = Tier1CRMTestDataFactorySetup.getDataFactory();

	private static T1C_Base__Team__c t1;
	private static Account acc;
	private static T1C_Base__Employee__c emp;
	private static T1C_Base__Account_Coverage__c accCoverage2;
	private static T1C_Base__Account_Coverage_Additional_Info__c acaiRecord;
	private static T1C_Base__Account_Coverage_Additional_Info__c acaiRecord2;
    private static final String AFR_BASE_FEATURE = 'ACE.CubeConfig.CoverageCube';

    //  ========================================================================================
//  Method: setupAFRTree
//  Desc:  Method that will create the following features: ACE.CubeConfig.CoverageCube
//  ========================================================================================
    private static void setupData()
    {
    	T1C_FR__Feature__c ace = new T1C_FR__Feature__c(Name = 'ACE',T1C_FR__Name__c = 'ACE');
		T1C_FR__Feature__c cubeCon = new T1C_FR__Feature__c(Name = 'CubeConfig',  T1C_FR__Name__c = 'ACE.CubeConfig');
        T1C_FR__Feature__c covCube = new T1C_FR__Feature__c(Name = 'CoverageCube',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube');
        T1C_FR__Feature__c ac = new T1C_FR__Feature__c(Name = 'AccountCoverage',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage');
        T1C_FR__Feature__c cc = new T1C_FR__Feature__c(Name = 'ContactCoverage',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.ContactCoverage');
        T1C_FR__Feature__c acLayout = new T1C_FR__Feature__c(Name = 'Layout',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout');
        T1C_FR__Feature__c ccLayout = new T1C_FR__Feature__c(Name = 'Layout',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.ContactCoverage.Layout');
        T1C_FR__Feature__c formCol = new T1C_FR__Feature__c(Name = 'FormColumns',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns');
        T1C_FR__Feature__c aciaCol = new T1C_FR__Feature__c(Name = 'FormACIColumns',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormACIColumns');
        T1C_FR__Feature__c aciaProd = new T1C_FR__Feature__c(Name = 'ProductField',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormACIColumns.ProductField');
        T1C_FR__Feature__c aciaRole = new T1C_FR__Feature__c(Name = 'RoleField',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormACIColumns.RoleField');
        T1C_FR__Feature__c empNameFld = new T1C_FR__Feature__c(Name = 'EmployeeNameField',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.EmployeeNameField');
        T1C_FR__Feature__c actNameFld = new T1C_FR__Feature__c(Name = 'AccountNameField',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.AccountNameField');
        T1C_FR__Feature__c fldCon = new T1C_FR__Feature__c(Name = 'FieldConfig',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.EmployeeNameField.FieldConfig');
        T1C_FR__Feature__c cols = new T1C_FR__Feature__c(Name = 'Columns',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.EmployeeNameField.FieldConfig.Columns');
        T1C_FR__Feature__c nameCol = new T1C_FR__Feature__c(Name = 'Name',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.EmployeeNameField.FieldConfig.Columns.Name');
        T1C_FR__Feature__c roleFld = new T1C_FR__Feature__c(Name = 'RoleField',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.RoleField');
		T1C_FR__Feature__c stDate = new T1C_FR__Feature__c(Name = 'StartDate',  T1C_FR__Name__c = 'ACE.CubeConfig.CoverageCube.AccountCoverage.Layout.FormColumns.StartDate');
		T1C_FR__Feature__c Core = new T1C_FR__Feature__c(	Name = 'Core', 						T1C_FR__Name__c = 'ACE.Core');
		T1C_FR__Feature__c asu = new T1C_FR__Feature__c(	Name = 'AceShareUtil', 				T1C_FR__Name__c = 'ACE.Core.AceShareUtil');
		T1C_FR__Feature__c obj = new T1C_FR__Feature__c(	Name = 'Objects', 					T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects');
		T1C_FR__Feature__c acct = new T1C_FR__Feature__c(	Name = 'Account', 					T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Account');
		T1C_FR__Feature__c aAcct = new T1C_FR__Feature__c(	Name = 'Account', 					T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Account.Account');
		T1C_FR__Feature__c cont = new T1C_FR__Feature__c(	Name = 'Contact', 					T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Contact');
		T1C_FR__Feature__c cCont = new T1C_FR__Feature__c(	Name = 'Contact', 					T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Contact.Contact');
		T1C_FR__Feature__c ActSec = new T1C_FR__Feature__c(	Name = 'Activity_Security__c', 		T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c');
		T1C_FR__Feature__c aActSec = new T1C_FR__Feature__c(Name = 'ActivitySecurity', 			T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.Activity_Security__c.ActivitySecurity');
		T1C_FR__Feature__c cr = new T1C_FR__Feature__c(		Name = 'T1C_Base__Call_Report__c', 	T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Call_Report__c');
		T1C_FR__Feature__c callRep = new T1C_FR__Feature__c(Name = 'T1C_Base__Call_Report__c', 	T1C_FR__Name__c = 'ACE.Core.AceShareUtil.Objects.T1C_Base__Call_Report__c.T1C_Base__Call_Report__c' );
		
    	upsert new list<T1C_FR__Feature__c>{ace,cubeCon,covCube,ac,cc,acLayout,ccLayout,formCol,aciaCol,aciaProd,aciaRole,empNameFld,actNameFld,fldCon,cols,nameCol,roleFld,stDate,Core,asu,obj,acct,aAcct,ActSec,aActSec,cr,callRep,cont,cCont};
                
		insert new list<T1C_FR__Attribute__c>
		{
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Id'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aAcct.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Account'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Account__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aActSec.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Activity_Security__c'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'T1C_Base__Client__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Account'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = callRep.Id, Name = 'ObjectName', T1C_FR__Value__c = 'T1C_Base__Call_Report__c'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'AccessLevel', T1C_FR__Value__c = 'Edit'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'BatchOnly', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'FilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'ParentCoverageFilterClause', T1C_FR__Value__c = ''),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'ParentLookupField', T1C_FR__Value__c = 'Id'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'ParentObjectType', T1C_FR__Value__c = 'Contact'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'RowCause', T1C_FR__Value__c = 'Manual'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cCont.Id, Name = 'ObjectName', T1C_FR__Value__c = 'Contact'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'DataField', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = nameCol.Id, Name = 'Style', T1C_FR__Value__c = ' width: 60%; color: #19198C;'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = fldCon.Id, Name = 'AdditionalWhereClause', T1C_FR__Value__c = 'T1C_Base__Inactive__c = FALSE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = fldCon.Id, Name = 'SearchFields', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = fldCon.Id, Name = 'SObjectName', T1C_FR__Value__c = 'T1C_Base__Employee__c'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ac.Id, Name = 'FieldLabel', T1C_FR__Value__c = 'Account Coverage'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ac.Id, Name = 'OrderByColumns', T1C_FR__Value__c = 'EmployeeNameField:1;AccountNameField:2'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = ac.Id, Name = 'OrderBy', T1C_FR__Value__c = '1'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cc.Id, Name = 'FieldLabel', T1C_FR__Value__c = 'Contact Coverage'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cc.Id, Name = 'OrderByColumns', T1C_FR__Value__c = 'EmployeeNameField:1;'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = cc.Id, Name = 'OrderBy', T1C_FR__Value__c = '1'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = acLayout.Id, Name = 'sObjectType', T1C_FR__Value__c = 'T1C_Base__Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = acLayout.Id, Name = 'displayACAI', T1C_FR__Value__c = 'TRUE'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaCol.Id, Name = 'sObjectType', T1C_FR__Value__c = 'T1C_Base__Account_Coverage_Additional_Info__c'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Employee__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'FieldType', T1C_FR__Value__c = 'LOOKUP'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'Label', T1C_FR__Value__c = 'Employee Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'Order', T1C_FR__Value__c = '0'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'ParentSObjectField', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'sObjectName', T1C_FR__Value__c = 'T1C_Base__Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = empNameFld.Id, Name = 'Visible', T1C_FR__Value__c = 'True'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Account__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'FieldType', T1C_FR__Value__c = 'LOOKUP'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'Label', T1C_FR__Value__c = 'Account Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'Order', T1C_FR__Value__c = '0'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'ParentSObjectField', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'sObjectName', T1C_FR__Value__c = 'T1C_Base__Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = actNameFld.Id, Name = 'Visible', T1C_FR__Value__c = 'True'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Product__c	'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'FieldType', T1C_FR__Value__c = 'LOOKUP'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'Label', T1C_FR__Value__c = 'Product'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'Order', T1C_FR__Value__c = '0'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'ParentSObjectField', T1C_FR__Value__c = 'Name'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaProd.Id, Name = 'Visible', T1C_FR__Value__c = 'True'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'ParentSObjectField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'FieldType', T1C_FR__Value__c = 'PICKLIST'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'Label', T1C_FR__Value__c = 'Role'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'Order', T1C_FR__Value__c = '1'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = aciaRole.Id, Name = 'Visible', T1C_FR__Value__c = 'True'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Start_Date__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'FieldType', T1C_FR__Value__c = 'DATE'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'Label', T1C_FR__Value__c = 'Start Date'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'Order', T1C_FR__Value__c = '0'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'sObjectName', T1C_FR__Value__c = 'T1C_Base__Account_Coverage__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = stDate.Id, Name = 'Visible', T1C_FR__Value__c = 'True'),
			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'Clone', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'DataField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'ParentSObjectField', T1C_FR__Value__c = 'T1C_Base__Role__c'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'FieldType', T1C_FR__Value__c = 'PICKLIST'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'IsInherited', T1C_FR__Value__c = 'false'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'IsRequired', T1C_FR__Value__c = 'TRUE'),			
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'Label', T1C_FR__Value__c = 'Role'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'Order', T1C_FR__Value__c = '1'),
			new T1C_FR__Attribute__c(T1C_FR__Feature__c = roleFld.Id, Name = 'sObjectName', T1C_FR__Value__c = 'T1C_Base__Account_Coverage__c')
    	};

        User user = new User (id = UserInfo.getUserId());
        
        acc = dataFactory.makeAccount('testAccount');
		insert acc;
		
		Contact contact = dataFactory.makeContact(acc.Id, 'Marla', 'Singer');
		insert contact;

        emp = dataFactory.makeLoggedInUserEmployee();
		T1C_Base__Employee__c emp2 = dataFactory.makeEmployee('Test', 'Employee2', null);
		insert new List<T1C_Base__Employee__c> { emp, emp2 };


        T1C_Base__Account_Coverage__c accCoverage = dataFactory.makeAccountCoverage(acc.Id, emp.Id);
        accCoverage.T1C_Base__Unique_Id__c  = acc.Id + ':' + emp.Id + ':null:null';
		accCoverage2 = dataFactory.makeAccountCoverage(acc.Id, emp2.Id);
        accCoverage2.T1C_Base__Unique_Id__c  = acc.Id + ':' + emp2.Id + ':null:null';
        insert new List<T1C_Base__Account_Coverage__c>{ accCoverage, accCoverage2 };

		T1C_Base__Contact_Coverage__c ccCoverage = dataFactory.makeContactCoverage(contact.Id, emp.Id);
        insert ccCoverage;

        t1 = new T1C_Base__Team__c();
        insert t1; 

        T1C_Base__Team_Member__c tmc = new T1C_Base__Team_Member__c(T1C_Base__Team__c = t1.id, T1C_Base__Employee__c = emp.id);
        insert tmc;

        acaiRecord = new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__Account_Coverage__c = accCoverage.id, T1C_Base__Team__c = t1.id);
		acaiRecord2 = new T1C_Base__Account_Coverage_Additional_Info__c(T1C_Base__Account_Coverage__c = accCoverage2.id, T1C_Base__Team__c = t1.id);
        insert acaiRecord;
	}

    static testMethod void SetUpNewAccountCoverage()
    {
        setupData();

        map<string, string> coverageRecordDetails = new map<string,string>();
        coverageRecordDetails.put('coverageType', 'Account Coverage');
        coverageRecordDetails.put('createCoverage', 'true');
		coverageRecordDetails.put('recordId', string.ValueOf(acc.id));

        CoverageFormCoreController.getConfig(JSON.serialize(coverageRecordDetails));
        CoverageFormCoreController.getACAIRecords(JSON.serialize(coverageRecordDetails));

    }
 
    static testMethod void SetUpEditAccountCoverage()
    {
        setupData();

        map<string, string> acaiRecordMap = new map<string, string>();
        acaiRecordMap.put('id', acaiRecord.Id);
        acaiRecordMap.put('T1C_Base__Team__c', t1.Id);

		map<string, string> newAcaiRecordMap = new map<string, string>();
		newAcaiRecordMap.put('id', 'newACAIRecord');
		newAcaiRecordMap.put('T1C_Base__Account_Coverage__c', accCoverage2.id);
		newAcaiRecordMap.put('T1C_Base__Team__c', t1.Id);
		newAcaiRecordMap.put('Inactive__c', String.valueOf(false));
        
        list<map<string,string>> acaiRecordList = new list<map<string,string>>();
        acaiRecordList.add(acaiRecordMap);
		acaiRecordList.add(newAcaiRecordMap);



        map<string, string> coverageRecordDetailsMap = new map<string,string>();
        coverageRecordDetailsMap.put('coverageType', 'Account Coverage');
        coverageRecordDetailsMap.put('createCoverage', 'false');
        coverageRecordDetailsMap.put('uniqueId', string.valueOf(acc.Id) + ':' + string.valueOf(emp.id) + ':' + 'null:null');
        coverageRecordDetailsMap.put('recordId', string.ValueOf(acc.id));
        coverageRecordDetailsMap.put('sObjectType', 'T1C_Base__Account_Coverage__c');

		Test.startTest();
        CoverageFormCoreController.getConfig(JSON.serialize(coverageRecordDetailsMap));
        
        
        CoverageFormCoreController.getACAIRecords(JSON.serialize(coverageRecordDetailsMap));


        map<string,string> fi = new map<string, string>();
        fi.put('T1C_Base__Account__c', acc.id);
        fi.put('T1C_Base__Division__c', 'testtest');
        
        string formItems = JSON.serialize(fi);
        string coverageRecordDetails = JSON.serialize(coverageRecordDetailsMap);
        string acaiRecords  = JSON.serialize(acaiRecordList);

        formItems = formItems.replace('"','\\"');
        coverageRecordDetails = coverageRecordDetails.replace('"','\\"');
        acaiRecords = acaiRecords.replace('"','\\"');
       
        string formOptions = '{' + '"' + 'coverageRecordDetails' + '":"' + coverageRecordDetails + '","' + 'formItems":"' + formItems +  '","' + 'acaiRecords":"' +acaiRecords + '"}';

        CoverageFormCoreController.SaveCoverageForm(formOptions);
		
		Boolean requireAddInfoForAccntCov = CoverageFormCoreController.requireAddInfoForAccntCov;
		Boolean requireAddInfoForContCov = CoverageFormCoreController.requireAddInfoForContCov;
		String additionalInfoErrorMessage = CoverageFormCoreController.additionalInfoErrorMessage;

        Test.stopTest();

    }
	
}