//=============================================================================
/**
 * Name: CoverageFormItemFactory.js
 * Description: Form Item Factory for Quick Add Spcific types.
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{

	var _super = ACE.inherit(CoverageFormItemFactory, ACE.DynamicFormItemFactory);
	var regionField; 

	/**
	 * Constructor
	 */
	function CoverageFormItemFactory()
	{
		_super.constructor.call(this);
	}


  CoverageFormItemFactory.prototype.TEXT = function(config, options)
  {
    var currentOptions = $.extend({}, options);
    if (currentOptions.parentContainerClass !== undefined)
    {
      currentOptions.parentContainerClass += ' TEXT';
    }

    return DynamicFormItemFactory.TEXT(config, currentOptions);

  }

   	//  ==================================================================
 	//  Method: Employee Lookup 
 	//  Description: Updates the datagrid based on the employee selected
 	//  ==================================================================
	CoverageFormItemFactory.prototype.EMPLOYEELOOKUP = function(config, options)
	{
		//_this           = options._this;
		//configOptions   = options;
		var editControl = $('<input type="text"></input>');

		var formItem = new DynamicFormItem(config, options, editControl);

		var autoComplete = new SearchAutoComplete(editControl,
		{				
			mode                 : "CUSTOM:" + config.additionalConfig.id,
			config               : config.additionalConfig,
			maintainSelectedItem : true,
			additionalQueryFields:["Region__c", "Region_text__c", "IndustryGroup__c"], // adding this to see what happens
			coverageOnly         : false		
		});

		formItem.setVal = function(val)
		{
			autoComplete.setSelectedItem(val, function()
			{
				formItem.updateLabelMask();
			});
		};

		formItem.addChangeHandler = function()
		{
			autoComplete.set("select", function(item)
			{
				ACE.CoverageFormAddDialog.prototype.updateRegion(item);
				ACE.CoverageFormAddDialog.prototype.updateIndustry(item);

				formItem._changeHandler();								
			});
		};

		formItem.getVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Id;
			}
			return "";
		};


		formItem.getSelectedItem = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem;
			}
			return null;

		};

		formItem.getFormattedVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Name;
			}
			return "";
		};

		formItem.addChangeHandler();
		return formItem;
	};





//  ==================================================================
 	//  Method: REGION Lookup 
 	//  Description: Updates the datagrid based on the employee selected
 	//  ==================================================================
	CoverageFormItemFactory.prototype.REGIONLOOKUP = function(config, options)
	{
		//_this           = options._this;
		//configOptions   = options;
		var editControl = $('<input type="text"></input>');

		var formItem = new DynamicFormItem(config, options, editControl);

		var autoComplete = new SearchAutoComplete(editControl,
		{				
			mode                 : "CUSTOM:" + config.additionalConfig.id,
			config               : config.additionalConfig,
			maintainSelectedItem : true,
			coverageOnly         : false		
		});

		formItem.setVal = function(val)
		{			
			autoComplete.setSelectedItem(val, function()
			{
				formItem.updateLabelMask();
				//formItem._changeHandler();
			});
		};

		formItem.addChangeHandler = function()
		{
			autoComplete.set("select", function(item)
			{
				formItem._changeHandler();
			});
		};

		formItem.getVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Id;
			}
			return "";
		};


		formItem.getSelectedItem = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem;
			}
			return null;

		};

		formItem.getFormattedVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Name;
			}
			return "";
		};

		formItem.addChangeHandler();
		regionField = formItem;
		return formItem;
	};


  /*CoverageFormItemFactory.prototype.ACCOUNT = function(config, options)
	{
    var currentOptions = $.extend({}, options);
		//var editControl = $('<input type="text" readonly></input>');
    //var editControl = $('<input type="text" readonly><apex:form id="accountLookup"><apex:inputField style="width: 95%" styleClass="optionFont" id="accountId" value="{!contactCoverage.T1C_Base__Contact__c}" required="true"/></apex:form></input>');
		//var editControl = $('<apex:form id="accountLookup"><apex:inputField style="width: 100%" id="accountId" value="{!contactCoverage.T1C_Base__Contact__c}" required="true"/></apex:form>');
		var editControl = $('<table style="width: 50%;align:center"><tr><td class="label" style="width:40%">Account</td><td class="cell" style="width: 60%"><apex:form id="accountLookup"><apex:inputField style="width: 95%" styleClass="optionFont" id="accountId" value="{!contactCoverage.T1C_Base__Contact__c}" required="true"/></apex:form></td></tr></table>');

		var formItem = new DynamicFormItem(config, currentOptions, editControl);
		return formItem;
		//return DynamicFormItemFactory.TEXT(config, currentOptions,editControl);


  }*/


	/*CoverageFormItemFactory.prototype.ACCOUNT = function(config, options)
	{
		if(options.coverageOnly == true && options.excludedAccounts !=undefined)
		{
			config.tooltip = 'Only returns Accounts within your Account Coverage and your available Account Types';
		}
		else if(options.coverageOnly == undefined && options.excludedAccounts !=undefined)
		{
			config.tooltip = 'Only returns Accounts within your available Account Types';
		}
		else if(options.coverageOnly == true && options.excludedAccounts ==undefined)
		{
			config.tooltip = 'Only returns Accounts within your Account Coverage';
		}
		var editControl = $('<input type="text"></input>');
		var formItem = new DynamicFormItem(config, options, editControl);

		$('#qacInheritCheckbox').change(function() {
			formItem._changeHandler();
		});

		var _isChanging = false;
		var objectField;
		config.dataField = 'AccountId';


		if (formItem._isDataBinding)
		{
			if (config.dataField.indexOf('__c') != -1)
			{
				objectField = config.dataField.replace('__c', '__r');
			}
			else if (config.dataField.indexOf('Id') != -1)
			{
				objectField = config.dataField.replace('Id', '');
			}
		}

		var autoComplete = new SearchAutoComplete(editControl,
		{
			mode: "ACCOUNTS",
			config: config.additionalConfig,
			maintainSelectedItem: true,
			coverageOnly: options.coverageOnly,
			listCSS:"quickAddContactAutoComplete",
			customFilterFunction:removeExcludedAccounts
		});

		function removeExcludedAccounts(item, searchString)
		{
			if(options.excludedAccounts!=undefined && options.excludedAccounts.indexOf(item.RecordTypeId) != -1){
				return false;
			}
			else
			{
				return true;
			}
		}

		//TODO: Add ability to retrieve the selected item based off the item
		//Override the set val to retrieve the object based off the selected item Id
		formItem.setVal = function(val)
		{
			var dp = options.dataProvider;

			if (typeof val === "string" && objectField != null && dp[objectField] != null)
			{
				var o = dp[objectField];

				if (o != null && o.Id == val)
				{
					val = dp[objectField];
				}
			}
			else if (typeof val === "object")
			{
				//If they're assigning the object to the lookup __c or Id field we'll re-point it back to the relationship field
				//and re-assign the data back to the id
				dp[objectField] = val;
				//Assume its a SObject with Id value
				dp[config.dataField] = val.Id;
				val = val.Id;
			}
			autoComplete.setSelectedItem(val, function()
			{
				//Since the update to the owner is async we'll update it after it comes back
				formItem.updateLabelMask();
				formItem._changeHandler();
			});

		};

		formItem.addChangeHandler = function()
		{
			autoComplete.set("select", function(item)
			{
				formItem._changeHandler();

				if (formItem._isDataBinding && objectField != null)
				{
					options.dataProvider[objectField] = item;
				}
			});
		};

		formItem.getVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Id;
			}
			return "";
		};

		formItem.getSelectedItem = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem;
			}
			return null;
		};

		formItem.getFormattedVal = function()
		{
			if (autoComplete.selectedItem !== undefined && autoComplete.selectedItem !== null)
			{
				return autoComplete.selectedItem.Name;
			}
			return "";
		};
		formItem.addChangeHandler();
		if(options.accountId != undefined)
		{
			formItem.setVal(options.accountId);
		}

		if(options.showAccount == false)
		{
			editControl.hide();
			formItem.labelControl.hide();
		}
		return formItem;
	};
*/
/*
	CoverageFormItemFactory.prototype.LAYOUT = function(config, options)
	{
		var parentControl = $('<div class="hbox"></div>');
		var formItems = [];
		$.each(config.children, function( index, item ) {
			  formItems.push(_super.append(parentControl,item,options));
		});

		return [parentControl,formItems];
	};
*//*
	$(document).mouseup(function(e) 
	{
		var container = $("#ui-datepicker-div");
		var hasClass = $(e.target).hasClass('.hasDatepicker');
		var hasElemenet = $.contains( container, e.target ); 
		//hack - the multiselect is not a child of the container, so if it is clicked, we assume that it is in the container.
		if (container.id != e.target.id && hasElemenet == false)// &&  container.has(e.target).length === 0 && container != undefined) 
		{
			if(container != undefined)
			{
				if(container.style == undefined)
				{
					if(container[0].style.display == 'none')
					{
						$(container).show();
					}
					else
					{
						$(container).hide();
					}
				}
				else if(container.style.display == 'none')
				{
					$(container).show();
				}
				else
				{
					$(container).hide();	
				}
			}
		}
	});
*/
	/*Specific case for Expiry dates when syncing to ACAI is enabled*/
	CoverageFormItemFactory.prototype.EXPIRYDATE = function(config, options)
	{
		var formItem = CoverageFormItemFactory.prototype.DATE(config, options);

		//Trigger an expiry date changed event when the date is changed
		formItem.afterCloseHandler = function()
		{
			ACE.CustomEventDispatcher.trigger('syncACAIEndDates', { endDate : formItem.getVal()});
		};

		//When the ACAI triggers a change, sync up the end date to the date that is determined by the grid
		ACE.CustomEventDispatcher.on('setCoverageExpiryDate', function(e, args)
		{
			var isExpired = true;
			var acais = args.acais;
			var expiryField = args.expiryField;

			$.each(acais, function(index, acai)
			{
				var acaiExpired = acai[expiryField] || acai.T1C_Base__End_Date__c <= Date.now();
				isExpired = isExpired && acaiExpired;
			});

			if(!isExpired)
			{
				return;
			}

			console.log("AC Syncing to ACAI end date");
			var curEndDate = formItem.getVal();
			
			if(!curEndDate || curEndDate != args.endDate)
			{
				formItem.setVal(args.endDate);
				formItem.updateLabelMask(true);
			}
		});

		return formItem;
	}

	CoverageFormItemFactory.prototype.DATE = function(config, options)
	{
		var currentOptions = $.extend({}, options);
		
		var requireFieldClass = '';
		if(config.isRequired == true || config.isRequired == 'true')
		{
			requireFieldClass = 'requiredField';
		}
		
		if (currentOptions.parentContainerClass !== undefined)
		{
			currentOptions.parentContainerClass += ' Date ' + requireFieldClass;
			//currentOptions.labelMaskClass= requireFieldClass;
			currentOptions.showLabelMask = true;
		}
		var editControl = $('<input type="text" class="' + requireFieldClass + '" readonly></input>');
		var formItem = new DynamicFormItem(config, currentOptions, editControl);

		var originalAppend = formItem.append;
		
		var datePicker = editControl.datepicker
		({
			showButtonPanel: true,
			closeText: 'Clear',
			dateFormat: "D M dd, yy",
			//Hijack the close button of the date picker to be clear
			beforeShow: function(input)
			{
				var isReadOnly = false;

				if (options.readOnly != null)
				{
					if (typeof options.readOnly	=== "function")
					{
						isReadOnly = options.readOnly(formItem);
					}
					else
					{
						isReadOnly = options.readOnly;
					}
				}

				if (!isReadOnly)
				{
					setTimeout(function()
					{
						var clearButton = $(input)
							.datepicker( "widget" )
							.find( ".ui-datepicker-close" );
							clearButton.unbind("click").bind("click", function()
							{
								$.datepicker._clearDate( input );
							});
					}, 1 );
				}
				else
				{
					return false;
				}
			},
			//Add a custom onClose handler to hide the label mask if it's visible...
			onClose: function(dateText, inst)
			{
				formItem.updateLabelMask(true);
				formItem.afterCloseHandler(dateText, inst);
			}
		});

		if(config.dateValue != undefined)
		{
			datePicker.datepicker("setDate", new Date(config.dateValue));
		}

		/**
		 * Override the set val to ensure we convert the retrieved value into a js date object
		 *
		 * @param val to Assign
		 */
		formItem.setVal = function(val)
		{
			if (typeof val === "number")
			{
				//Assign it as a UTC date... again cause it's in timezone of the user
				editControl.datepicker("setDate", ACE.DateUtil.getUTCDate(val));
				formItem._changeHandler();
			}
		};

		/**
		 * Gets the formatted value to display in the label mask (if it's there). Since we don't want the date in milliseconds
		 * and want an actual format we override this
		 */
		formItem.getFormattedVal = function()
		{
			return editControl.val();
		};

		/**
		 * Override the add change handler as it doesn't fire the onChange event so add the custom event
		 */
		formItem.addChangeHandler = function()
		{
			editControl.datepicker("option", "onSelect", function()
			{
				formItem._changeHandler();

				if (options.showLabelMask)
				{
					setTimeout(function()
					{
						//Refocus on the label mask but ensure it doesn't refocus back on the editable element
						//this is prevent it from recursively opening the date picker
						//TODO: Fix the multiselect picklist and date picker to handle recursion better
						formItem._labelMask.focus(false);
					}, 100);
				}
			});
		};

		/**
		 * Override the internal change handler to ensure the label mask is updated
		 */
		formItem._changeHandler = function()
		{
			formItem._defaultChangeHandler();
			//Since the label mask object updates the label on focus loss of the element
			//update it on actual change of the date picker as focus out is too early and the data hasn't been updated yet
			formItem.updateLabelMask(true);
		};


		//Override the getVal to handle date picker
		formItem.getVal = function()
		{
			var d = editControl.datepicker("getDate");

			if (d !== null && d !== undefined)
			{
				//HACK: Setting time to noon on Date items instead of midnight avoids date switching when utc convert.
				/*if(d.getHours() == 0)
				{
					d.setHours(12);
				}*/
				//Convert it to a UTC date for saving to the database
				//return ACE.DateUtil.getUTCDate(d).getTime();
				//return d.getTime();
				return d.getTime() - d.getTimezoneOffset()*60000;
			}
			return "";
		};

		//An after close handler to be overridden by implementors
		formItem.afterCloseHandler = function(dateString, dateObject)
		{
			return;
		}

		return formItem;
	};

	$.extend(true, window,
	{
		"ACE":
		{
			"CoverageFormItemFactory":new CoverageFormItemFactory()
		}
	});

//# sourceURL=CoverageFormItemFactory.js
})(jQuery);
