//=============================================================================
/**
 * Name: ACE.QuickAddDialog.js
 * Description: Base Component for Quick Add Dialogs
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
	$.extend(true, window,
	{
		"ACE":
		{
			"CoverageFormAddDialog": CoverageFormAddDialog
		}
	});

  	var _this;
  	var _options;


	var BASE_OPTIONS =
	{
		//The SObject data provider to use... if not specified the form populates with the basic information
		dataProvider: null,
		//Callback handler to call after save. Passes SObject to the callback
		saveCallback: null,
		//Flag to determine if the dialog should automatically save the Task to Salesforce on clicking the Done or Save button
		autoSave: true,
	};

	var BASE_FORM_OPTIONS =
	{
		parentContainerClass: "parentContainer",
		editControlContainerClass: "editControlContainer",
		showLabelMask: false,
		editControlLabelClass: "editControlLabel"
	};


	var _super = ACE.inherit(CoverageFormAddDialog, ACE.AbstractDialog);

	
	/**
	 * Constructor
	 */
	function CoverageFormAddDialog(options)
	{
		_this = this;
		BASE_OPTIONS['buttons']=
			[{
				id:"addAdditionalInfoButton",
				text: "Add Products",
				"class":"confirmButton",
				click: function()
				{
					_this.AdditionalCoverageInfo.additionalInfoFormGrid.grid.getEditController().commitCurrentEdit();
					_this.AdditionalCoverageInfo.addAdditionInfoRecord();
				}
			},
			{
				id:"quickAddSaveButton",
				text: "Save And Close",
				"class":"confirmButton",
				click: function()
				{
					_this.saveClickHandler();
				}
			},
			{                                                                                     
				id:"quickAddCancelButton",
				text: "Cancel",
				click: function()
				{
          			window.close();
				},
				"class":"cancelButton"
			}];
		if(options != null && options != undefined)
		{
			options = $.extend({}, BASE_OPTIONS, options);
		}
		 
		_super.constructor.call(this, options);

	}


	//  ==================================================================
	//  Method: show
	//  Description: Overload the show method to accept the options parameter
	//  ==================================================================
	CoverageFormAddDialog.prototype.show = function(opts)
	{
		_options = $.extend({}, BASE_OPTIONS, opts);
		this.options = opts;
		_super.show.call(this, _options);
	};


	//  ==================================================================
	//  Method: init
	//  Description: Get config
	//  ==================================================================
	CoverageFormAddDialog.prototype.init = function()
	{
		_this.getConfig();
	};

  	//  ==================================================================
  	//  Method: getConfig
  	//  Description: Get the configuration from apex
  	//  ==================================================================
	CoverageFormAddDialog.prototype.getConfig = function()
	{
		
		var data = {"coverageRecordDetails": JSON.stringify(_options['coverageRecordDetails'])};

		var result = sforce.apex.execute("CoverageFormCoreController", "getConfig", data, 
				{ onSuccess: function(result){

					_this.isInitialized = true;
					_this._config = JSON.parse(result);
		 			_this._parseConfig(JSON.parse(result));
				}, 
				onFailure: function(error){
					var test = error;
				} 
		});
	}

 	//  ==================================================================
 	//  Method: createForm
 	//  Description: Creates any of the necessary display objects onto the DOM
 	//  ==================================================================
	CoverageFormAddDialog.prototype.createForm = function(config,options)
	{
		options = $.extend({}, BASE_FORM_OPTIONS, options);

		this._form = new DynamicForm(options,ACE.CoverageFormItemFactory);
		this._formItemList = this._form.append(_options.baseTarget, config.fieldFormFields);
		this._internalShow();

		//show additionalInfo coverage form 
		if(config.displayACAI)
     	{
     		 this.AdditionalCoverageInfo = new ACE.AdditionalCoverageInfo();
     		 this.AdditionalCoverageInfo.showAdditionalInfoGrid(config,options.coverageRecordDetails);
     	}
     	else 
	    {
	      $('#addAdditionalInfoButton').hide();
	    }
	};

 	//  ==================================================================
 	//  Method: validate
 	//  Description: Validates the form before save
 	//  @return true if the form validates, false otherwise
 	//  ==================================================================
	CoverageFormAddDialog.prototype.validate = function()
	{
		var isValid = true;

		if (this._formItemList != null)
		{
			$.each(this._formItemList.requiredItems, function(index, item)
			{
				if (!item.isValid())
				{
					isValid = false;
					return false;
				}
			});
		}
		return isValid;
	};

 	//  ==================================================================
 	//  Method: getConfig
 	//  Description: Handles when the save or done button was pressed
 	//  ==================================================================
	CoverageFormAddDialog.prototype.saveClickHandler = function()
	{
		_this.itemToAdd = {};
		_this.configuredFields = [];

		//saves any edit that has not been committed yet on the addiontial coverage infomation form 
		if(_this._config.sObjectType == 'T1C_Base__Account_Coverage__c' && _this.AdditionalCoverageInfo != null)
		{
			_this.AdditionalCoverageInfo.additionalInfoFormGrid.grid.getEditController().commitCurrentEdit();
			_this.acaiRecords = _this.AdditionalCoverageInfo.additionalInfoFormGrid.grid.getData().getItems();
		}
		else
		{
			_this.acaiRecords = [];
		}

    	var isValid = true; 
		if (!this.validate())
		{
			isValid = false;
			var warningString = "Fields highlighted in red are required. Fill in the fields and then save.";
			ACEConfirm.show
			(
				'<p>'+warningString+'</p>',
				"Warning",
				[
					"OK"
				],
				function(result)
				{
				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
			);
			return;
		}

		$.each(this._formItemList.items, function(index, item)
		{
			if(/*item.getVal() !=null && item.getVal() != '' &&*/ item._config.isEditable == true)
			{
				_this.itemToAdd[item._config.dataField] = item.getVal();
				_this.configuredFields.push(item._config.dataField);
			}
		});
		
		if(_this.acaiRecords != null)
		{
			$.each(_this.acaiRecords, function(index, item)
			{
				isValid = false;

				$.each(item, function(key, value)
				{
					if(key != 'id' && value != null && value != '')
					{
						isValid = true;
					}
				});	
				
				if(!isValid)
				{
					ACEConfirm.show
					(
						'<p>At least one Additional Information field must be filled in before saving.</p>',
						"Warning",
						[
							"OK"
						],
						function(result)
						{
						},
						{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
					);
					return;
				}
			});
		}
		/*
		warningString = "A coverage record must have at least one active product. If you wish to expire an entire coverage relationship please add an end date on the form.";
		var isOneProductActive = false

		if(_this._config.sObjectType == 'T1C_Base__Account_Coverage__c')
		{
			$.each(_this.acaiRecords, function(index, item)
			{
				if(!item.Inactive__c)
				{
					isOneProductActive = true;
				}	
			});
		}
		else
		{
			isOneProductActive = true;
		}
		
		if(!isOneProductActive)
		{
			ACEConfirm.show
			(
				'<p>'+warningString+'</p>',
				"Warning",
				[
					"OK"
				],
				function(result)
				{
				},
				{resizable: false, height:250, width:500, dialogClass: "warningDialog"}
			);
			return;
		}
		*/
		
		//ST-3138 Converted to a callback because this wasnt considering if an error occured
		var saveCallBack = function(e, args)
		{
			var deleteACAIRecords =   ACE.AdditionalCoverageInfo.prototype.getDeletedACAIRecords();
			$.each(deleteACAIRecords, function(index, item)
			{
				sforce.connection.deleteIds(item);
			});
		};

		if(isValid)
		{
			this._saveForm(_this.itemToAdd, _this._config.sObjectType, _this.acaiRecords, saveCallBack);
		}
	};

	//  ==================================================================
	//  Method: updateRegion
	//  Description: update region given the employee
	//  ==================================================================
	CoverageFormAddDialog.prototype.updateRegion = function(emp)
	{
		if (emp == null)
		{
			return; 
		}
		var formItemList = _this._formItemList;

		for (var i = 0 ; i < formItemList.items.length; i++)
		{
			if (formItemList.items[i]._config.dataField == "Coverage_Region__c")
			{
				formItemList.items[i].setVal(emp.Region__c);
			}
		}
	};

	// todo - add industry here
	//  ==================================================================
	//  Method: updateIndustry
	//  Description: update industry given the employee
	//  ==================================================================
	CoverageFormAddDialog.prototype.updateIndustry = function(emp)
	{
		if (emp == null)
		{
			return; 
		}
		var formItemList = _this._formItemList;

		for (var i = 0 ; i < formItemList.items.length; i++)
		{
			if (formItemList.items[i]._config.dataField == "IndustryGroup__c")
			{
				formItemList.items[i].setVal(emp.IndustryGroup__c);
			}
		}
	};

	//# sourceURL=CoverageFormAddDialog.js
})(jQuery);
