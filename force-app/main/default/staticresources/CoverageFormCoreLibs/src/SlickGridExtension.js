//=============================================================================
/**
 * Name: SlickGridExtension.js
 * Description: Adding editors to the slick grid component 
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{

    $.extend(true, window, {
	    "Slick": {
	      "Editors": {
	        "AutoComplete": AutoCompleteEditor,
	        "Picklist": PickListEditor,
            "Checkbox": CheckboxEditor    
	      }
	    }
	});


    function AutoCompleteEditor(args) {
     var myInput;
     this.args = args;
     var _this;

     this.init = function () {
        myInput = $p('<la-input />');
        myInput.appendTo(args.container);
    	setTimeout(function()
		{
	       	var autoComplete = new SearchAutoComplete($(myInput[0]),
			{				
				mode: "CUSTOM:" + args.column.additionalConfig.id,
				config: args.column.additionalConfig,
				coverageOnly: false,
				select: function(value)
				{
					_this.applyValue(value); 
				}		
			});
        },5);
				
     };

     this.loadValue = function (item) {
        if(item[args.column.field] != null)
        {
            $(myInput[0]).val(item[args.column.field]);
        }
        else if(item[args.column.field.substring(0,args.column.field.indexOf("."))] != null)
        {
            $(myInput[0]).val(item[args.column.field.substring(0,args.column.field.indexOf("."))].Name);
        }
        else
        {
           $(myInput[0]).val = "";
        }
     
     	this.item = item;
     	_this = this;

     }

     this.serializeValue = function () {
     	//return $(myInput[0]).val();
     }

     this.isValueChanged = function () {
     	 //return !($(myInput[0]).val() == ""); 
     }

    this.destroy = function () {
      	$(myInput[0]).remove();
     };

     this.applyValue = function (value) {

        //updates the datagrid view with the selected name
     	_this.item[_this.args.column.field] = value.Name;

        //update the fields in "item" so that we can save the ids to salesforce 
        var field = _this.args.column.field;
        _this.args.item[field.substring(0,field.indexOf("."))] = value;
        field = field.substring(0,field.indexOf("__r")).concat("__c");
        _this.args.item[field] = value.Id;
     	_this.args.commitChanges();
     };

    this.validate = function() {

	};

     this.init();
   }


   function PickListEditor(args)
   {
        var _this = this;
        var menuButton;
        this.init = function() {
            var fieldName = args.column.field;
            var cellValue = args.item[fieldName];

            if(cellValue == undefined)
            {
                cellValue = '';
            }

            var picklistOptions = args.column.picklistOptions;
            var picklistValues = [];
            if(picklistOptions != undefined)
            {
                picklistValues.push({data : '', label : 'Please select an option',defaultSelected : (cellValue == '')});
                for(var x=0;x<picklistOptions.length;x++)
                {
                    var plVal = picklistOptions[x];
                    picklistValues.push({data : plVal, label : plVal,defaultSelected : (cellValue == plVal)});
                }
            }
                
            menuButton = new ACEMenuButton($(args.container),{ dataProvider : picklistValues});

            // ST-4904: Apply the value early so that it isn't reset upon resizing the grid
            menuButton._container.on("itemSelected", function(e, eventData)
            {
                _this.applyValue(args.item, eventData.item.data); 
            });

            return menuButton;
        }

        this.destroy = function() {
            //menuButton.remove();
        };

        this.focus = function() {
            menuButton.focus();
        };

        this.loadValue = function(item) {
            defaultValue = item[args.column.field];
            menuButton.val(defaultValue);
        };

        this.serializeValue = function() {
            if(args.column.picklistOptions){
              return menuButton.val();
            }
        };

        this.applyValue = function(item,state) {
            item[args.column.field] = state;
        };

        this.isValueChanged = function() {
            return (menuButton.val() != defaultValue);
        };

        this.validate = function() {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
   }

   function CheckboxEditor(args)
   {
        var _this = this;
        var checkbox;
        var defaultValue;

        this.init = function() {   
            checkbox = $("<input type='checkbox' class='primaryLabel'>");
            checkbox.appendTo(args.container);      
            checkbox.on("click", function(e)
            {
                ACE.CustomEventDispatcher.trigger('expiryCheckboxClicked', { 
                    item: args.item,
                    expire: checkbox.prop("checked")
                });
            });

            _this.focus();

            return checkbox;
        }

        this.destroy = function() {
            checkbox.remove();
        };

        this.focus = function() {
            checkbox.focus();
        };

        this.loadValue = function(item) {
            defaultValue = item[args.column.field];
            checkbox.prop("checked", defaultValue);
        };

        this.serializeValue = function() {
            return checkbox.prop("checked");
        };

        this.applyValue = function(item, state) {
            item[args.column.field] = state;
        };

        this.isValueChanged = function() {
            return (_this.serializeValue() !== defaultValue);
        };

        this.validate = function() {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
   }

   /*
    function PickListEditor(args) {
        var $select;
        var defaultValue = args.item[args.column.field];
        var _this = this;

        this.init = function() {

        	opt_values = args.column.picklistOptions;
            option_str = ""
            for(var i = 0; i < opt_values.length; i++){
              v = opt_values[i];
              option_str += "<OPTION value='"+v+"'>"+v+"</OPTION>";
              //option_str += "<li class='aceMenuItem' data-value='"+v+"'>"+v+"</li>";
            }
            $select = $("<SELECT tabIndex='0' style='width:100%;' class='editor-select'>"+ option_str +"</SELECT>");
            //$select = $("<ul data-type='aceMenu' class='buttonMenu aceMenu'>"+ option_str +"</ul>");
            $select.appendTo(args.container);
            //$select.val(defaultValue);
            $select.focus();
        };

        this.destroy = function() {
            $select.remove();
        };

        this.focus = function() {
            $select.focus();
        };

        this.loadValue = function(item) {
            defaultValue = item[args.column.field];
            $select.val(defaultValue);
        };

        this.serializeValue = function() {
            if(args.column.picklistOptions){
              return $select.val();
            }
        };


        this.applyValue = function(item,state) {
            item[args.column.field] = state;
        };

        this.isValueChanged = function() {
            return ($select.val() != defaultValue);
        };

        this.validate = function() {
            return {
                valid: true,
                msg: null
            };
        };

        this.init();
    }*/
	//# sourceURL=SlickGridExtensions.js
})(jQuery);