//=============================================================================
/**
 * Name: CoverageForm.js
 * Description: Javascript controller used for managing the CoverageForm
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document is shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
   var _super = ACE.inherit(CoverageForm, ACE.CoverageFormAddDialog);
   var _this;
   var _options;

   function CoverageForm()
   {

    String.prototype.replaceAll = function(search, replacement) 
    {
        var target = this;
        return target.split(search).join(replacement);
    };

    //Append coverage form to the div tags
    this.body = $('body');
    this.coverageFormDialog = $('<div class="coverageFormDialog"></div>').appendTo(this.body);
    this.coverageForm = $('<div class="coverageForm"></div>').appendTo(this.coverageFormDialog);

    //establish connection using session ID
    sforce.connection.sessionId = ACE.Salesforce.sessionId;

    var BASE_OPTIONS = {
      width: "auto",
      height: "auto",
      autoSave: true,
      target: this.coverageFormDialog,
      baseTarget:this.coverageForm,
      dialogClass: "quickAddContactDialog ps-coverage-form-grid",
      controller: "CoverageFormCoreController"
     };

          
    BASE_OPTIONS['coverageRecordDetails'] = {
      recordId: ACEUtil.getURLParameter('recordId'),
      createCoverage: ACEUtil.getURLParameter('createCoverage'),
      coverageType: ACEUtil.getURLParameter('coverageType').replace('%20',' ').replace('+', ' '),
      uniqueId: ACEUtil.getURLParameter('uniqueId') == null ? ACEUtil.getURLParameter('uniqueId') : ACEUtil.getURLParameter('uniqueId').replaceAll('%20', ' ')
     };

    var mode; 
    if(BASE_OPTIONS['coverageRecordDetails'].createCoverage == 'false')
    {
       mode = 'Edit';
    }
    else
    {
       mode = 'Create';
    }
    BASE_OPTIONS['title'] = mode + ' ' +BASE_OPTIONS['coverageRecordDetails'].coverageType;     

    _options = $.extend({}, BASE_OPTIONS, _options);
    _options.dataProvider = {};
    _super.constructor.call(this, BASE_OPTIONS);

    this.show();
   }

  //  ==================================================================
 	//  Method: _parseConfig
 	//  Description: Parase the config
 	//  ==================================================================
   CoverageForm.prototype._parseConfig = function(config)
   {
     this.createForm(config, _options);
     this._setDefaultValues(); 
   }

  //  ==================================================================
  //  Method: _setDefaultValues
  //  Description: Set default values on form popup
  //  ==================================================================
   CoverageForm.prototype._setDefaultValues = function()
   {
     $.each(this._formItemList.items, function(index, item)
     {
       if(item._config !== undefined)
       {
         if(item._config.fieldType == "DATE" || item._config.fieldType == "EXPIRYDATE")
         {
           item.setVal(item._config.dateValue);
         }
         else
         {
           item.setVal(item._config.defaultValue);
         }

       }
     });

   }


  //  ==================================================================
  //  Method: _saveForm
  //  Description: To save the form values
  //  ==================================================================
   CoverageForm.prototype._saveForm = function(formItemsToSave, sObjectType, acaiRecords, callBack)
   {
      var saveOptions = _options['coverageRecordDetails'];
      saveOptions['sObjectType'] = sObjectType;

      //remove inner objects in acaiRecords so that we can map this to the salesforce list of map<string,string>
      ACE.LoadingIndicator.show('Saving...');
      if(sObjectType == 'T1C_Base__Account_Coverage__c')
      {
        $.each(acaiRecords, function(index, item)
        {
          for(var propt in item)
          {
            // Update the end date with the timezone offset to avoid it being sent back a day
            if (propt === "T1C_Base__End_Date__c")
            {
              if (item[propt] && item[propt].getHours !== 0)
              {
                item[propt] = item[propt]  - new Date(item[propt]).getTimezoneOffset()*60000;
              }
            }
            else if (typeof(item[propt]) === 'object' && item[propt] != null)
            {
              delete item[propt];
            }
            else if(propt.substring(0,propt.indexOf("__r")))
            {
              delete item[propt];
            }
          }
        });
      }


      //stringfy the options
      var formOptions = {"formOptions": JSON.stringify({"coverageRecordDetails": JSON.stringify(saveOptions), "formItems": JSON.stringify(formItemsToSave), "acaiRecords": JSON.stringify(acaiRecords)})};

      var result = sforce.apex.execute("CoverageFormCoreController", "SaveCoverageForm", formOptions, 
        { onSuccess: function(result)
          {
			ACE.CustomEventDispatcher.trigger('refreshCubePane', { cubeFaceIdList : ['HTMLCoverageCube']});
          var messageString, modalType, mode;
          ACE.LoadingIndicator.hide();
             if (result[0].includes('Success: '))
             { 
               modalType = 'Success';
               if(_options['coverageRecordDetails'].createCoverage == 'false')
               {
                 mode = 'Edit';
               }
               else
               {
                 mode = 'Creation';
               }
                messageString = result[0].replace('Success: ','');
                callBack();
             }
             else
             {
               modalType = 'Warning';
               messageString= "Error: " + result;
             }
            ACEConfirm.show
            (
             '<p>'+messageString+'</p>',
             modalType,
             [
               "OK::confirmButton"
             ],
             function(result)
             {
               if(result == 'OK' && modalType == 'Success')
               {
                  window.close();
               }
             },
             {resizable: false, height:250, width:500, dialogClass: "warningDialog"}
            );

          }, 
        onFailure: function(error){
            ACE.LoadingIndicator.hide();  
        } 
    });
   }

  //  ==================================================================
  //  Method: show
  //  Description: Show the Coverage Form
  //  ==================================================================
   CoverageForm.prototype.show = function()
   {
     _super.show.call(this, _options);
   }

    $.extend(true, window,
    {
        "ACE":
        {
            "CoverageForm": new CoverageForm()
        }
    });

//# sourceURL=CoverageForm.js
})(jQuery);
