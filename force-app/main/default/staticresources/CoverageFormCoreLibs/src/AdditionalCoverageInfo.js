//=============================================================================
/**
 * Name: ACE.AdditionalCoverageInfo.js
 * Description: Additional Coverage Info on the Coverage Form
 *
 * Confidential & Proprietary, 2015 Tier1CRM Inc.
 * Property of Tier1CRM Inc.
 * This document shall not be duplicated, transmitted or used in whole
 * or in part without written permission from Tier1CRM.
 */
//=============================================================================
(function($)
{
  var _this;
  var deleteACAIRecordIds = [];
  var dateColumnMap = {};
  
  $(document).ready(function () {
     $(document).on('click', '.del', function () {
        
    var me = $(this), id = me.attr('id');
    var messageString = 'Are you sure you want to delete this product?'
    
    //ACE.LoadingIndicator.show('Deleting...');
    ACEConfirm.show
    (
     '<p>'+messageString+'</p>',
     'Warning',
     [
       "Yes::confirmButton","Cancel::cancelButton"
     ],
     function(result)
     {

      if(result == 'Yes')
      {
          
        //delete the selected cell
        var grid = _this.additionalInfoFormGrid.grid;
        var data= grid.getData();
        data.deleteItem(id);

        //remove record from salesforce
        if(!id.startsWith("newACAIRecord"))
        {
          var ids = [id];
          deleteACAIRecordIds.push(ids);
          //var DeleteResult = sforce.connection.deleteIds(ids);            
        }
      }
     },
     {resizable: false, height:250, width:500, dialogClass: "warningDialog"}
    );
      }); 
  });   

	function AdditionalCoverageInfo()
	{
		//add to the divs
		this.coverageFormParentContainer = $('<div class="parentContainer Grid"><label>Product Information</label></div>').appendTo('.coverageForm');
	  this.additionalInfoForm = $('<div class="additionalInfoForm"></div>').appendTo(this.coverageFormParentContainer);
	}

  AdditionalCoverageInfo.prototype.addAdditionInfoRecord = function()
  {
    _this.newACAIRecord = {id:"newACAIRecord:" + Date.now()};
    _this.newACAIRecord = $.extend({}, _this.newACAIRecord, _this.ACAIDefaultValues);
    _this.additionalInfoFormGrid.addItems(_this.newACAIRecord);
  } 

  AdditionalCoverageInfo.prototype.updateAdditionInfoRegionField = function(employeeID)
  {
    var query = "SELECT T1C_Base__Product_Lookup__c, T1C_Base__Product_Lookup__r.Name FROM T1C_Base__Employee__c WHERE id='" + employeeID + "'";
    var result = sforce.connection.query(query);
    if(result.records != null || result.records != undefined)
    { 
      if(result.records.T1C_Base__Product_Lookup__r != null && result.records.T1C_Base__Product_Lookup__c != null)
      {
        _this.ACAIDefaultValues['T1C_Base__Product__c'] = result.records.T1C_Base__Product_Lookup__c;
        _this.ACAIDefaultValues['T1C_Base__Product__r.Name'] = result.records.T1C_Base__Product_Lookup__r.Name;
      }
      else
      {
        _this.ACAIDefaultValues['T1C_Base__Product__c'] = '';
        _this.ACAIDefaultValues['T1C_Base__Product__r.Name'] = '';
      }
    } 
  }

  AdditionalCoverageInfo.prototype.getDeletedACAIRecords = function()
  {
    return deleteACAIRecordIds;
  }

	AdditionalCoverageInfo.prototype.showAdditionalInfoGrid = function(config,coverageRecordDetails)
  {
      var isInit = false;//Latch to prevent the expiry logic from firing on init
      function getDefaultValues()
      {
        var defaultValues = {};

        if(config.gridFormFields.length > 0)
        {          
          $.each(config.gridFormFields, function(index, item)
          {
            if(item.fieldType == "LOOKUP")
            {
              defaultValues[item.dataField] = item.defaultId;
              defaultValues[item.dataField.replace('__c', '__r.Name')] = item.defaultValue;              
            }
            else if(item.fieldType == "PICKLIST")
            {          
              defaultValues[item.dataField] = item.defaultValue;
            }
            else
            {
              defaultValues[item.dataField] = item.defaultValue;
            }                  
          });
        }
        return defaultValues;
      }

    function CheckboxBOX(row, cell, value, columnDef, dataContext) {
        var checked = '';
        if(value)
        {
            checked = 'checked="checked"';
        }

        return "<input type='checkbox' id='isActive-ID-" + dataContext.id
        + "' class='primaryLabel' " + checked + ">";
    }


    function getColumns()
    {
        var columns = [];  
        if(config.gridFormFields.length > 0)
        {
          var removeButtonFormat = function(row,cell,value,columnDef,dataContext)
          {  
            //return "<input class='del' type='text' value='Remove' id='"+ dataContext.id +"' />";
            return "<input class='del' type='submit' value='Remove' id='" + dataContext.id + "'/>";
          }
          
          var removeButton =  {id: "removeButton", name: "", field: "Remove",formatter: removeButtonFormat, width: 50, resizable: false};

            $.each(config.gridFormFields, function(index, item)
            	{
					var o = {};
					if(item.fieldType == "LOOKUP")
					{
						o = {
							id: item.id,
							field: item.dataField.replace('__c', '__r.Name'),
							width: item.width,
							name:item.label,
							sortable: true,
							fieldType: "STRING",
              editable: true,
              sortable: false,
							additionalConfig: item.additionalConfig,
              editor: Slick.Editors.AutoComplete,
              formatter: lookupFormatter
							};      						
					}
					else if(item.fieldType == "PICKLIST")
					{
						o = {
							id: item.id,
							field: item.dataField,
							minwidth: item.width,
							name:item.label,
							sortable: true,
							picklistOptions: item.picklistOptions,
							fieldType: "STRING",
              editable: true,
              sortable: false,
              editor: Slick.Editors.Picklist,
              formatter: picklistFormatter
						  };
				  } 
				 else if(item.fieldType == "CHECKBOX")
				  {
					o = {
              id: item.id,
              field: item.dataField,
              width: item.width,
              name:item.label,
              sortable: true,
              fieldType: "STRING",
              editable: true,
              sortable: false,
              editor: Slick.Editors.Checkbox,
              formatter: CheckboxBOX  
            };
          }
				 else if(item.fieldType == "DATE")
          {
            o = {
              id: item.id,
              field: item.dataField,
              width: item.width,
              name:item.label,
              sortable: true,
              fieldType: "Date",
              editable: true,
              sortable: false,
              formatter: dateFormatter,
              editor: Slick.Editors.Date  
          };
				  
				  dateColumnMap[item.dataField] = o;
                 }
                 else
                 {
                    o = {
                      id: item.id,
                      field: item.dataField,
                      width: item.width,
                      name:item.label,
                      sortable: true,
                      sortable: false,
                      fieldType: "STRING",
                      editable: true,
                      editor: Slick.Editors.Text  
                  };
                 }

            			columns.push(o);

            });

            if(config.addRemoveButton)
            {
                columns.push(removeButton);                
            }
        }

        return columns;
    }

    this.additionalInfoForm = $('.additionalInfoForm'); 
    this.additionalInfoFormGrid = new ACEDataGrid(this.additionalInfoForm , {  
        maxNumberOfRows:10, 
        autoHeight: true, 
        forceFitColumns: true, 
        explicitInitialization: true,
        editable: true,
        editOnClick :true,
        headerRowHeight: 50,
        autoEdit:false
    });

    this.additionalInfoFormGrid.setColumns(getColumns());
    
    /*Sync the expiry dates when the user clicks off the date editor. This is the most consistent way I have to do it*/
    this.additionalInfoFormGrid.grid.onCellChange.subscribe(function(e, args)
    {
        if(config.syncAIEndDates && isInit)
        {
            var dataContext = _this.additionalInfoFormGrid.dataProvider.getItem(args.row);

            //Figure out which column is changing.
            var curColumn = args.grid.getColumns()[args.cell].field;
            //Determine which field triggered the change
            var isExpired = (curColumn == "T1C_Base__End_Date__c" && dataContext.T1C_Base__End_Date__c <= Date.now())
                || (curColumn == config.expiryField && dataContext[config.expiryField]);

            //HACK toggle the opposite field based on the field that triggered the expiry change
            //Also should support un-expiring
            if(curColumn == "T1C_Base__End_Date__c")
            {
                dataContext[config.expiryField] = dataContext.T1C_Base__End_Date__c <= Date.now();
            }

            window.setTimeout(function(e, args)
            {
                _this.additionalInfoFormGrid.dataProvider.updateItem(dataContext.id, dataContext);
            });

            //Find the highest end date and trigger the set coverage expiry
            ACE.CustomEventDispatcher.trigger('setCoverageExpiryDate', { 
                endDate : dataContext.T1C_Base__End_Date__c, //The proposed End date
                acais : _this.additionalInfoFormGrid.dataProvider.getItems(),
                expiryField : config.expiryField
            });
        }

    }.bind(this));

    // Expiry checkbox should update the end date immediately (this event is called directly from the checkbox editor)
    ACE.CustomEventDispatcher.on('expiryCheckboxClicked', function(e, args)
    {
      var item = args.item;

      item.T1C_Base__End_Date__c = args.expire ? Date.now() : null;
      item[config.expiryField] = args.expire;
      _this.additionalInfoFormGrid.dataProvider.updateItem(item.id, item);

      //Find the highest end date and trigger the set coverage expiry
      ACE.CustomEventDispatcher.trigger('setCoverageExpiryDate', { 
        endDate : item.T1C_Base__End_Date__c, //The proposed End date
        acais : _this.additionalInfoFormGrid.dataProvider.getItems(),
        expiryField : config.expiryField
      });
    });

    //When the AC Form triggers a change to its End Date, sync up the end date to the date that is determined by the grid
    ACE.CustomEventDispatcher.on('syncACAIEndDates', function(e, args)
    {
        var newEndDate = ACE.DateUtil.getUTCDate(args.endDate);

        if(!newEndDate)
        {
            return;
        }

        console.log("ACAI Syncing to end date");
        this.additionalInfoFormGrid.dataProvider.beginUpdate();

        //Scan each item, if it's end date is null or is greater than the AC's expiry date, set it equal to the AC's end date
        $.each(this.additionalInfoFormGrid.dataProvider.getItems(), function(index, item)
        {
            var acaiEndDate = item.T1C_Base__End_Date__c;
            var expire = newEndDate <= Date.now();
            var id = item.id;
            if(!acaiEndDate || acaiEndDate > newEndDate)
            {
                item.T1C_Base__End_Date__c = newEndDate;
                item[config.expiryField] = expire;
                this.additionalInfoFormGrid.dataProvider.updateItem(id, item);
            }
        }.bind(this));

        this.additionalInfoFormGrid.dataProvider.endUpdate();

    }.bind(this));

    this.ACAIDefaultValues = getDefaultValues(); 
    _this = this;

    setTimeout(function(){
      _this.additionalInfoFormGrid.grid.init();
    },1);

    $(window).resize(function() {
      _this.additionalInfoFormGrid.grid.resizeCanvas();
    });
    
       //get additional coverage infomation records      
    var data = {"coverageRecordDetails": JSON.stringify(coverageRecordDetails)};
    sforce.apex.execute("CoverageFormCoreController", "getACAIRecords", data, 
    { onSuccess: function(result){

      if(result == "")
      {
        _this.ACAIRecords = [{}];
      }
      else
      {
        _this.ACAIRecords  = JSON.parse(result);
        $.each(_this.ACAIRecords, function(index, item)
        {
          //acedatagrid will not show the record due to the uppercasing in "Id". Making a copy of the Id to a lower case "id"
          item["id"] = item["Id"];
          for (var property in item) 
          {
            if (item.hasOwnProperty(property)) 
            {
              if(dateColumnMap.hasOwnProperty(property))
              {
                var propVal = item[property];
                item[property] = new Date(new Date(propVal).getTime() + new Date().getTimezoneOffset() * 60000);
              }
            }
		      }
		  
          delete item["Id"];
        });
		
		_this.additionalInfoFormGrid.addItems(_this.ACAIRecords);
        _this.additionalInfoFormGrid.grid.resizeCanvas();
        isInit = true;

      }
    }, 
    onFailure: function(error){
        
      } 
    });  

  } 

  function picklistFormatter(row, cell, value, columnDef, dataContext)
  {
    if (value == null || value == '')
    {
      var blankSpan =  '<span class="fakeSelect"></span>'
      return blankSpan.toString();
    }
    else
    {
      var blankSpan =  '<span class="fakeSelect">' + value +'</span>'
      return blankSpan.toString();
    }
  }
  
    function lookupFormatter(row, cell, value, columnDef, dataContext)
    {
        var returnSpan = '<span class="fakeInput">' + value + '</span>';
        return returnSpan.toString();
    }

	function dateFormatter(row, cell, value, columnDef, dataContext)
	{			
        if (value == null || value == '')
        {
        var blankSpan =  '<span class="fakeDatePicker"></span>'
        return blankSpan.toString();
        } 
        else if(typeof value == 'string')
        {
        value = Number(value);
        }
        
        var dateSpan = '<span class="fakeDatePicker">' + new Date(value).toLocaleDateString() + '</span>';
		return dateSpan.toString(); 
	}

	$.extend(true, window,
	{
		"ACE":
		{
			"AdditionalCoverageInfo": AdditionalCoverageInfo
		}
	});
//# sourceURL=AdditionalCoverageInfo.js
})(jQuery);	